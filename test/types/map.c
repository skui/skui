/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#include "../test.h"
#include "skui/types/map.h"


int
skui_test_map_set(void)
{
  skui_map_t map;
  skui_string_t value;

  skui_map_create(&map);

  skui_map_set(&map, "hello", "world");
  value = skui_map_get(&map, "hello");

  SKUI_TEST_STRCMP(value, "world");

  skui_map_set(&map, "hello", "planet");
  value = skui_map_get(&map, "hello");

  SKUI_TEST_STRCMP(value, "planet");

  skui_map_free(&map);

  return 1;
}


SKUI_TESTS = {
  SKUI_TEST_CASE(skui_test_map_set),
  SKUI_TEST_LASTCASE
};
