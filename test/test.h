/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_TEST_H__
#define __SKUI_TEST_H__


#include <stdlib.h>
#include <stdio.h>


typedef int (*skui_test_fcn)(void);


typedef struct skui_test_case_ {
  char* name;
  skui_test_fcn fcn;
} skui_test_case;


skui_test_case skui_tests[];


#define SKUI_TESTS skui_test_case skui_tests[]
#define SKUI_TEST_CASE(x) { #x, x }
#define SKUI_TEST_LASTCASE { NULL, NULL }


#define SKUI_TEST_ASSERT(x) {                                           \
    if (!(x))                                                           \
      {                                                                 \
        fprintf(stderr, "FAIL: %s (%s:%d)\n", #x, __FILE__, __LINE__);  \
        return 0;                                                       \
      }                                                                 \
  }

#define SKUI_TEST_EQd(x, y) {                                   \
    if ((x) != (y))                                             \
      {                                                         \
        fprintf(stderr, "FAIL: %d != %d (%s:%d)\n", x, y,       \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }

#define SKUI_TEST_EQlud(x, y) {                                 \
    if ((x) != (y))                                             \
      {                                                         \
        fprintf(stderr, "FAIL: %lud != %lud (%s:%d)\n", x, y,   \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }

#define SKUI_TEST_STRCMP(x, y) {                                \
    if (strcmp((x), (y)))                                       \
      {                                                         \
        fprintf(stderr, "FAIL: %s != %s (%s:%d)\n", x, y,       \
                __FILE__, __LINE__);                            \
        return 0;                                               \
      }                                                         \
  }


int main(int argc, char** argv)
{
  int i = 0;

  skui_test_case* test = skui_tests;

  if (argc > 1)
    i = atoi(argv[1]);

  for (; test->name; i++, test++)
    {
      if (!test->fcn())
        {
          fprintf(stderr, "Test %s (%i) failed\n", test->name, i);
          return i + 1;
        }
    }

  return 0;
}


#endif
