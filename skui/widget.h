/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_WIDGET_H__
#define __SKUI_WIDGET_H__


#include "skui/api.h"
#include "skui/widget_def.h"
#include "skui/types/map.h"
#include "skui/value.h"
#include "skui/value/string.h"
#include "skui/value/real.h"

#include <stdlib.h>


SKUI_API_CALL skui_value_t*
skui_widget_set_property(skui_map_t* properties,
                         skui_string_t key,
                         skui_value_t value)
{
  skui_map_kv_t* kv = skui_map_get_kv(properties, key);

  if (!kv)
    {
      kv = skui_map_set(properties, key, NULL);
      kv->value = malloc(sizeof(skui_value_t));
    }

  *((skui_value_t*)kv->value) = value;

  return kv->value;
}


SKUI_API_CALL skui_widget_t*
skui_widget_create(skui_widget_t* widget,
                   skui_widget_t* parent,
                   skui_string_t id)
{
  skui_map_create(&widget->properties);

  skui_widget_set_property(&widget->properties,
                           "id",
                           skui_value_string_create(skui_string_dup(id), SKUI_TRUE));

  skui_widget_set_property(&widget->properties,
                           "x",
                           skui_value_real_create(0));

  skui_widget_set_property(&widget->properties,
                           "y",
                           skui_value_real_create(0));

  skui_widget_set_property(&widget->properties,
                           "width",
                           skui_value_real_create(0));

  skui_widget_set_property(&widget->properties,
                           "height",
                           skui_value_real_create(0));


  skui_map_create(&widget->events);

  skui_array_create(&widget->filters, sizeof(skui_string_t));
  skui_array_create(&widget->shapes, sizeof(skui_string_t));
  skui_array_create(&widget->layouts, sizeof(skui_string_t));

  widget->parent = parent;

  skui_array_create(&widget->children, sizeof(skui_widget_t*));


  return widget;
}


SKUI_API_CALL void
skui_widget_free(skui_widget_t* widget, skui_bool_t recursive)
{
  skui_map_kv_t* properties_iter;
  skui_widget_t* children_iter;

  for (properties_iter = (skui_map_kv_t*) widget->properties.start;
       properties_iter < (skui_map_kv_t*) widget->properties.end;
       properties_iter++)
    {
      skui_value_free(*(skui_value_t*)properties_iter->value);
      free(properties_iter->value);
    }

  skui_map_free(&widget->properties);


  skui_map_free(&widget->events);

  skui_array_free(&widget->filters);
  skui_array_free(&widget->shapes);
  skui_array_free(&widget->layouts);


  for (children_iter = (skui_widget_t*) widget->children.start;
       children_iter < (skui_widget_t*) widget->children.end;
       children_iter++)
    {
      if (recursive)
        skui_widget_free(children_iter, SKUI_TRUE);
      else
        children_iter->parent = NULL;
    }
}


#endif
