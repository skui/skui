/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_WIDGET_DEF_H__
#define __SKUI_WIDGET_DEF_H__


#include "skui/types/map_def.h"
#include "skui/types/array_def.h"


typedef struct skui_widget_t_ skui_widget_t;
typedef struct skui_widget_t_
{
  /*
    skui_map_t<skui_value_t>

    id:     skui_string_t

    x:      skui_pos_t
    y:      skui_pos_t
    width:  skui_pos_t
    height: skui_pos_t
  */
  skui_map_t properties;

  /* skui_map_t<skui_value_fcn_t> */
  skui_map_t events;

  /* skui_array_t<skui_string_t> */
  skui_array_t filters;

  /* skui_array_t<skui_string_t> */
  skui_array_t shapes;

  /* skui_array_t<skui_string_t> */
  skui_array_t layouts;

  skui_widget_t* parent;

  /* skui_array_t<skui_widget_t*> */
  skui_array_t children;
} skui_widget_t;


#endif
