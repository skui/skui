/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_TYPES_MAP_H__
#define __SKUI_TYPES_MAP_H__


#include "skui/api.h"
#include "skui/types/map_def.h"
#include "skui/types/array.h"
#include "skui/types/string.h"

#include <stdlib.h>
#include <string.h>


#define skui_map_copy skui_array_copy
#define skui_map_free skui_array_free


SKUI_API_CALL skui_map_t*
skui_map_create(skui_map_t* map)
{
  return skui_array_create(map, sizeof(skui_map_kv_t));
}


SKUI_API_CALL skui_ssize_t
skui_map_indexof(skui_map_t* map,
                 skui_string_t key)
{
  skui_map_kv_t* map_iter = map->start;

  for (map_iter = (skui_map_kv_t*)map->start;
       map_iter < (skui_map_kv_t*)map->end;
       map_iter++)
    {
      if (skui_string_eq(map_iter->key, key))
        return map_iter - (skui_map_kv_t*)map->start;
    }

  return -1;
}


SKUI_API_CALL skui_map_kv_t*
skui_map_get_kv(skui_map_t* map,
                skui_string_t key)
{
  skui_ssize_t index = skui_map_indexof(map, key);

  if (index < 0)
    return NULL;

  return skui_array_get(map, index);
}


SKUI_API_CALL void*
skui_map_get(skui_map_t* map,
             skui_string_t key)
{
  skui_map_kv_t* kv = skui_map_get_kv(map, key);

  if (!kv)
    return NULL;

  return kv->value;
}


SKUI_API_CALL void*
skui_map_set(skui_map_t* map,
             skui_string_t key,
             void* value)
{
  skui_map_kv_t* kv = skui_map_get_kv(map, key);
  skui_map_kv_t localkv;

  if (!kv)
    {
      localkv.key = key;
      localkv.value = value;

      return skui_array_push(map, &localkv);
    }
  else
    {
      kv->value = value;

      return kv;
    }
}


#endif
