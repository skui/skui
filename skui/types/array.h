/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_TYPES_ARRAY_H__
#define __SKUI_TYPES_ARRAY_H__


#include "skui/api.h"
#include "skui/types/array_def.h"
#include "skui/types/size.h"

#include <stdlib.h>
#include <string.h>


SKUI_API_CALL skui_array_t*
skui_array_create(skui_array_t* array,
                  skui_size_t element_size)
{
  array->start = NULL;
  array->end = NULL;

  array->length = 0;
  array->element_size = element_size;

  return array;
}


SKUI_API_CALL void*
skui_array_copy(skui_array_t* dest,
                skui_array_t* source)
{
  dest->start = malloc(source->end - source->start);
  dest->end = dest->start + (source->end - source->start);

  if (!dest->start)
    return NULL;

  memcpy(dest->start, source->start, source->length * source->element_size);
  dest->length = source->length;
  dest->element_size = source->element_size;

  return dest->start;
}


SKUI_API_CALL void
skui_array_free(skui_array_t* array)
{
  free(array->start);

  array->end = array->start;
  array->length = 0;
}


SKUI_API_CALL void*
skui_array_realloc(skui_array_t* array,
                   skui_size_t length)
{
  array->start = realloc(array->start, length * array->element_size);
  array->end = array->start + (length * array->element_size);
  array->length = length;

  return array->start;
}


SKUI_API_CALL void*
skui_array_get(skui_array_t* array,
               skui_size_t id)
{
  if (id >= array->length)
    return NULL;

  return array->start + (id * array->element_size);
}


SKUI_API_CALL void*
skui_array_get_value(skui_array_t* array,
                     skui_size_t id,
                     void* value)
{
  void* ptr = skui_array_get(array, id);

  if (!ptr)
    return NULL;

  if (!value)
    return NULL;

  memcpy(value, ptr, array->element_size);

  return ptr;
}


SKUI_API_CALL void*
skui_array_set(skui_array_t* array,
               skui_size_t id,
               void* element)
{
  if (id >= array->length)
    return NULL;

  if (element)
    return memcpy(array->start + (id * array->element_size),
                  element,
                  array->element_size);
  else
    return memset(array->start + (id * array->element_size),
                  0,
                  array->element_size);
}


SKUI_API_CALL void*
skui_array_push(skui_array_t* array,
                void* element)
{
  if (!skui_array_realloc(array, array->length + 1))
    return NULL;

  return skui_array_set(array, array->length - 1, element);
}


SKUI_API_CALL void*
skui_array_pop(skui_array_t* array)
{
  if (array->length == 0)
    return NULL;

  if (!skui_array_realloc(array, array->length - 1))
    return NULL;

  return array->start;
}


SKUI_API_CALL void*
skui_array_memmove(skui_array_t* array,
                   skui_size_t source,
                   skui_size_t dest,
                   skui_size_t length)
{
  return memmove(array->start + (dest * array->element_size),
                 array->start + (source * array->element_size),
                 (length - source) * array->element_size);
}


SKUI_API_CALL void*
skui_array_insert(skui_array_t* array,
                  skui_size_t id,
                  void* element)
{
  if (id > array->length)
    return NULL;

  if (id == array->length)
    return skui_array_push(array, element);

  if (!skui_array_realloc(array, array->length + 1))
    return NULL;

  skui_array_memmove(array, id, id + 1, array->length - 1);
  return skui_array_set(array, id, element);
}


SKUI_API_CALL void*
skui_array_remove(skui_array_t* array,
                  skui_size_t id)
{
  if (id > array->length)
    return NULL;

  if (id == array->length)
    return skui_array_pop(array);

  skui_array_memmove(array, id + 1, id, array->length);
  return skui_array_realloc(array, array->length - 1);
}


SKUI_API_CALL skui_ssize_t
skui_array_indexof(skui_array_t* array,
                   void* element)
{
  skui_ssize_t i;

  for (i = 0; i < (skui_ssize_t)array->length; i++)
    {
      if (!memcmp(array + (i * array->element_size), element,
                  array->element_size))
        return i;
    }

  return -1;
}


#endif
