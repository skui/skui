/*
  Copyright (c) 2016 sku

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef __SKUI_VALUE_FCN_H__
#define __SKUI_VALUE_FCN_H__


#include "skui/api.h"
#include "skui/value_def.h"
#include "skui/value/fcn_def.h"

#include <string.h>
#include <stdlib.h>


SKUI_API_CALL skui_value_t
skui_value_fcn_create(skui_value_fcn_fcn_t fcn,
                      void* arg)
{
  skui_value_fcn_t* fcn_value = malloc(sizeof(skui_value_fcn_t));

  skui_value_t value = {
    .type = "fcn",

    .value = fcn_value,
    .free = free
  };

  *fcn_value = (skui_value_fcn_t) {
    .fcn = fcn,
    .arg = arg
  };

  return value;
}


SKUI_API_CALL skui_value_t
skui_value_fcn_get(skui_value_t value, void* context)
{
  skui_value_fcn_t* fcn;

  while (1)
    {
      if (strcmp(value.type, "fcn"))
        break;

      fcn = value.value;
      value = fcn->fcn(context, fcn->arg);
    }

  return value;
}


#endif
