#!/bin/sh
cat "$@" | grep "Copyright (c)" >/dev/null 2>&1
[ $? -eq 0 ] && exit

cat `dirname "$0"`/../LICENSE.txt "$@" > /tmp/licenseme && mv /tmp/licenseme "$@"
